﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonFruitScript : MonoBehaviour {

    List<GameObject[]> allPaths;

    Dictionary<int, int> dictionaryForPaths;

    public GameObject[] _path1;
    public GameObject[] _path2;
    public GameObject[] _path3;
    public GameObject[] _path4;
    public GameObject[] _path5;
    public GameObject[] _path6;
    public GameObject[] _path7;
    public GameObject[] _path8;
    public GameObject[] _path9;
    public GameObject[] _path10;
    public GameObject[] _path11;
    public GameObject[] _path12;
    public GameObject[] _path13;
    public GameObject[] _path14;
    public GameObject[] _path15;

    private void Start()
    {
        allPaths = new List<GameObject[]>();
        dictionaryForPaths = new Dictionary<int, int>();

        allPaths.Add(_path1);
        allPaths.Add(_path2);
        allPaths.Add(_path3);
        allPaths.Add(_path4);
        allPaths.Add(_path5);
        allPaths.Add(_path6);
        allPaths.Add(_path7);
        allPaths.Add(_path8);
        allPaths.Add(_path9);
        allPaths.Add(_path10);
        allPaths.Add(_path11);
        allPaths.Add(_path12);
        allPaths.Add(_path13);
        allPaths.Add(_path14);
        allPaths.Add(_path15);
    }

    public void FindNearestPath(string roomName)
    {
        //count how many steps to destination
        int index = 0;
        for (int i = 0; i < allPaths.Count; i++)
        {
            
          foreach (var item in allPaths[i])
           {
                if (item.name == roomName)
                {
                    dictionaryForPaths.Add(i, index);
                }
                index++;
           }
           index = 0;
         }

        //return the nearest path index
        int _minValue = 9999;
        int pathKey = 0;
        foreach (var item in dictionaryForPaths)
        {
            if (item.Value < _minValue)
            {
                pathKey = item.Key;
            }
                
        }

        //active all gameObject steps
        ActiveNearestPath(pathKey); 
      }
        
      public void ActiveNearestPath(int pathKey)
       {
        GameObject[] a = allPaths[pathKey];
         foreach (var item in a)
         {
            item.GetComponent<MeshRenderer>().enabled = true;
         }
       // foreach (var item in _path2)
       // {
       //     item.GetComponent<MeshRenderer>().enabled = true;
       // }

       }

}

   

