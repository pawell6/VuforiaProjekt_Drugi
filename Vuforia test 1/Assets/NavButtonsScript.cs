﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class NavButtonsScript : MonoBehaviour,IInputClickHandler
{
    public string nameOfRoom;
    private GameObject _menuToDisActive;

    private void Start()
    {
        _menuToDisActive = GameObject.FindGameObjectWithTag("optionsMenu");     
    }

    public void OnInputClicked(InputEventData eventData)
    {
        //.SendMessage(nameOfRoom);
        GameObject.FindGameObjectWithTag("GM").SendMessage("SetRoomYouNeed",nameOfRoom);
        _menuToDisActive.SetActive(false);
    }




}
