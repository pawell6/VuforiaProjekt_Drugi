﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour {

    Transform positionOfObject;
    public GameObject naviPrefab;
    public GameObject navMenuPrefab;
    public GameObject whichMarkerisRecognized;
    public string whichRoomYouNeed;

    void AssignDataPosition(Transform pos)
    {
        positionOfObject = pos;
          
    }

    public void InstantiateNavigation()
    {
        Instantiate(naviPrefab, positionOfObject);
    }

    public void TurnOnNavMenu()
    {
        //navMenu.SetActive(true);
        Vector3 pos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z + 1f);
        Quaternion quat = Camera.main.transform.rotation;
        Instantiate(navMenuPrefab, pos, quat);
    }

    public void TurnOffNavMenu()
    {
        GameObject toDestroy = GameObject.FindGameObjectWithTag("optionsMenu");
        Destroy(toDestroy);
        // navMenu.SetActive(false);
    }

    public void SetRecognizedMarker(GameObject obj)
    {
        whichMarkerisRecognized = obj;
    }

    public void SetRoomYouNeed(string _string)
    {
        InstantiateNavigation();
        whichMarkerisRecognized = GameObject.FindGameObjectWithTag("DraMarker");

        
        whichRoomYouNeed = _string;
        GoNaviAlgorithm();
    }

    public void GoNaviAlgorithm()
    {
        // whichMarkerisRecognized.GetComponent<DragonFruitScript>().FindNearestPath(whichRoomYouNeed);
        whichMarkerisRecognized.GetComponent<DragonFruitScript>().ActiveNearestPath(1);
    }
}
